%define __jar_repack %{nil}
#%define debug_package %{nil}
%define _tmppath /home/rpmbuild/rpm/tmp
%define _name StoreOperationsAPIGateway
%define _download_link "https://nexus.global.tesco.org/nexus/service/local/artifact/maven/content?g=StoreOperationsAPIGateway.Dev&a=StoreOperationsAPIGateway&v=LATEST&r=52_InstoreCustomerPicking&e=jar"
#%define output %( /etc/init.d/tesco.soag.auto status)
#%define application_pid %(echo %{output} | grep --quiet "Java is running with pid:")

Name:         %{_name}
Version:      %{_version}
Release:        1%{?dist}
Summary:        StoreOperationsAPIGateway application rpm deployment for jar

Group:          Applications/System
License:        Restricted
URL:            https://rajivsharma.com
BuildArch: noarch



%description
First version of StoreOperationsAPIGateway


%prep
cd %{_sourcedir}
echo "Dowloading Latest Store Operation API Gateway Artifact"
#wget --content-disposition %{_download_link} > %{_sourcedir}/StoreOperationsAPIGateway.jar
#cp -r %{_tmppath}/ $RPM_BUILD_ROOT/
echo "Download successful" 

%build

%pre
touch /appl/store-operations-api-gateway/logs/rpm.log
echo "[`date`] ====== deploying %{_name} %{_version} ======" >> /appl/store-operations-api-gateway/logs/rpm.log 2>&1
if [ "$1" = "1" ]; then
 echo "[`date`] Stopping Store Operation API GateWay Service" >> /appl/store-operations-api-gateway/logs/rpm.log 2>&1
 sudo /etc/init.d/tesco.soag.auto stop >> /appl/store-operations-api-gateway/logs/rpm.log 2>&1 

  if [ "$?" != "0" ]; then
    echo "[`date`] XXXXXXXXXXX Service Not Stopped XXXXXXXXXXX" >> /appl/store-operations-api-gateway/logs/rpm.log 2>&1
	exit 1
  fi
 echo "[`date`] Store Operation API GateWay Service stopped" >> /appl/store-operations-api-gateway/logs/rpm.log 2>&1
 
 echo "[`date`] Removing Exsisting Artifact"  >> /appl/store-operations-api-gateway/logs/rpm.log 2>&1
 rm /appl/store-operations-api-gateway/StoreOperationsAPIGateway.jar  >> /appl/store-operations-api-gateway/logs/rpm.log 2>&1

elif [ "$1" = "2" ]; then
  
  echo "Upgrade application";
  echo "[`date`] Stopping Store Operation API GateWay Service" >> /appl/store-operations-api-gateway/logs/rpm.log 2>&1
  sudo /etc/init.d/tesco.soag.auto stop >> /appl/store-operations-api-gateway/logs/rpm.log 2>&1
  
  if [ "$?" != "0" ]; then
    echo "[`date`] XXXXXXXXXXX Service Not Stopped XXXXXXXXXXX" >> /appl/store-operations-api-gateway/logs/rpm.log 2>&1
	exit 1
  fi
  echo "[`date`] Store Operation API GateWay Service stopped" >> /appl/store-operations-api-gateway/logs/rpm.log 2>&1
  echo "[`date`] cleaning document root" >> /appl/store-operations-api-gateway/logs/rpm.log 2>&1
  
  echo "[`date`] Removing Exsisting Artifact" >> /appl/store-operations-api-gateway/logs/rpm.log 2>&1
  rm /appl/store-operations-api-gateway/StoreOperationsAPIGateway.jar >> $RPM_BUILD_ROOT/appl/store-operations-api-gateway/logs/rpm.log 2>&1
  
fi

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/appl/store-operations-api-gateway/
#touch $RPM_BUILD_ROOT/appl/store-operations-api-gateway/logs/rpm.log
#cp -r %{_sourcedir}/StoreOperationsAPIGateway.jar %{RPM_BUILD_ROOT}/appl/store-operations-api-gateway/StoreOperationsAPIGateway.jar
cp -r %{_tmppath}/StoreOperationsAPIGateway.jar $RPM_BUILD_ROOT/appl/store-operations-api-gateway/StoreOperationsAPIGateway.jar

%clean
rm -rf $RPM_BUILD_ROOT
rm -rf  %{_sourcedir}

%preun

%files
%defattr(-,root,root)
/appl/store-operations-api-gateway/StoreOperationsAPIGateway.jar
#/appl/store-operations-api-gateway/logs/rpm.log

%post
echo "[`date`] Starting Store Operation API GateWay Service" >> /appl/store-operations-api-gateway/logs/rpm.log 2>&1
sudo /etc/init.d/tesco.soag.auto start >> /appl/store-operations-api-gateway/logs/rpm.log 2>&1

if [ "$?" = "3" ]; then
    echo "[`date`] XXXXXXXXXXX Store Operation API GateWay Service not started XXXXXXXXXXX"  >> /appl/store-operations-api-gateway/logs/rpm.log 2>&1



	exit 3
  fi

#echo %{application_pid}
#if [ "$?" = "1" ]
#then
#  echo "XXXXXXXXXXX Store Operation API GateWay Service not started XXXXXXXXXXX"
#  exit 1
#fi

sleep 30
curl http://localhost:8080 > /dev/null 2>&1
if [ "$?" != "0" ]; then
   echo "[`date`] deployment unsuccessfull, manual intervension required"  >> /appl/store-operations-api-gateway/logs/rpm.log 2>&1
exit 3
fi
echo "[`date`] success"  >> /appl/store-operations-api-gateway/logs/rpm.log 2>&1 

echo "Kudos!! deployment successfull"

%changelog

